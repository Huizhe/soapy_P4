"""
Config File for python tomographic AO simulation.

This configuration file contains a python dictionary in which all simulation parameters are saved. The main dictionary ``simConfig``, is split into several sections corresponding to different parts of the simulation, each of which is itself another python dictionary

This file will be parsed and missing parameters will trigger warnings, either defaulting to a reasonable value or exiting the simulation. Python syntax errors in this file can stop the simulation from running, ensure that every pararmeter finishes with a comma, including the sub-dictionaries.
"""

import numpy

simConfiguration = {

"Sim":{
    "simName"       :  "sh_8x8",
    "logfile"       :   "sh_8x8.log",
    "pupilSize"     :   128, 
    "nGS"           :   2,
    "nDM"           :   2,
    "nSci"          :   1,
    "nIters"        :   500,
    "loopTime"      :   1/400.0,
    "reconstructor" :   "MVM_SeparateDMs", #_SeparateDMs

    "verbosity"     :   2,

    "saveCMat"      :   False,
    "saveSlopes"    :   True,
    "saveDmCommands":   False,
    "saveLgsPsf"    :   False,
    "saveSciPsf"    :   True,
    },

"Atmosphere":{
    "scrnNo"        :   4,
    "scrnHeights"   :   numpy.array([0, 5000, 10000, 15000]),
    "scrnStrengths" :   numpy.array([0.5, 0.3, 0.1, 0.1]),
    "windDirs"      :   numpy.array([0, 45, 90, 135]),
    "windSpeeds"    :   numpy.array([10, 10, 15, 20]),
    "wholeScrnSize" :   2048,
    "r0"            :   0.16,#0.16
    },

"Telescope":{
   "telDiam"        :   4.,  #Metres
   "obsDiam"        :   0, #Central Obscuration
   "mask"           :   "circle",
    },

"WFS":{
    "GSPosition"    :   [(0,0), (5,0)],
    "GSHeight"      :   [0,   90e3],
    "GSMag"         :   [0,    0],
    "nxSubaps"      :   [2,  10],
    "pxlsPerSubap"  :   [10,   10],
    "subapFOV"      :   [2.5, 5.5],#[2.5,   5.5],
   #"fftOversamp"   :   [3,3],
    "wavelength"    :   [1.06e-6]*2,
    "centMethod"    :   ["brightestPxl"]*2,#brightestPxl
    #"centThreshold" :   [0.1]*2,
    "photonNoise"   :   [True]*2,
    "lgs"           :   [False,True],
    "removeTT"      :   [False,True],
    #"propagationMode" :  ["Physical"]*2,

    },

#"LGS":{#
#
#    },
"LGS":{
    "uplink"            :   [True]*2,
    "pupilDiam"         :   [0.3]*2,
    "wavelength"        :   [1.06e-6]*2,
    "propagationMode"   :   ["Physical"]*2,#Geometric
    "height"            :   [90e3]*2,
    "elongationDepth"   :   [1000]*2,
    "elongationLayers"  :   [10]*2,
    "launchPosition"    :   [(0,0)]*2,
    },

"P4WFS": {
        "GSPosition"    : (0,0), #arc sec
        "height"        :   0,
         "P4pupilSize"  :  64,
            "z1"        : 10e3,
            "z2"        : 20e3,
            "a"         : 1.,
            "mag"       : 0,
            "wavelength": 1.06e-6,
            "I0"        : 0.05,
            "delta_z"   : 1000,
            "photoncounts": "nP4", ###P4: use LGS power; other: consistent with SH
    },

#"DM":{
#    "type"           :   ["TT","Zernike"],
#    "nxActuators"    :    [2, 79],
#    "svdConditioning":   [ 1e-5,0.05],
#    "closed"         :   [ False, False],
#    "gain"           :   [ 1, 0.2],
#    "iMatValue"      :   [1,  0.2],#[ 2e3, 500],
#    "wfs"            :   [ 0, 1],
#    },

"DM":{
    "type"           :   ["TT",  "Zernike"],
    "nxActuators"    :    [ 2,    79],
    "svdConditioning":   [ 1e-5,  0.05],
    "closed"         :   [ False,   False],
    "gain"           :   [ 1,   1],
    "iMatValue"      :   [ 1,   1], 
    "wfs"            :   [ 0,   1],
    },

"Science":{
    "position"      :   [(0,0)],
    "FOV"           :   [2.0],
    "wavelength"    :   [0.8e-6],
    "pxls"          :   [80],
    "fftOversamp"   :   [2],
    }
}